package com.example.employee.user;

import com.opencsv.exceptions.CsvValidationException;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface UserService {
    Boolean readCsv(MultipartFile file) throws IOException, CsvValidationException;

    Page<User> findAllUser(int pageNo, int size, double minSalary, double maxSalary, String sort);

    User createUser(User user);

    User updateUser(String id, User user);

    Boolean deleteUser(String id);
}
