package com.example.employee.user;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.*;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;


    @Override
    public Boolean readCsv(MultipartFile file) throws IOException, CsvValidationException {

        if(!Objects.equals(file.getContentType(), "text/csv")){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Please upload csv file");
        }

        CSVReader reader = new CSVReader(new InputStreamReader(file.getInputStream()));

        String[] headerLine = reader.readNext();

        //to check empty file
        if(headerLine == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "empty file");
        }

        //to check column numbers
        if(headerLine.length != 4){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Expected 4 columns, but found " + headerLine.length);
        }

        List<User> userList = new ArrayList<>();

        String[] nextRecord;
        while ((nextRecord = reader.readNext()) != null){

            //not to read comment
            if(nextRecord[0].startsWith("#")) continue;

            //to check salary format
            double salary = 0;
            DecimalFormat df = new DecimalFormat("#.##");
            try {
                salary = Double.parseDouble(nextRecord[3]);

                if(salary < 0.0){ //index is 3 since the csv header order is fixed
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "salary can't be less than 0.0");
                }

                String formattedSalary = df.format(salary);
                if (!formattedSalary.equals(nextRecord[3])) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "salary must be in maximum 2 decimal format");
                }

            }catch (NumberFormatException e){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "salary is not in correct format");
            }

            userList.add(new User(
                    nextRecord[0],
                    nextRecord[1],
                    nextRecord[2],
                    salary
            ));

        }

        //to check conflict
        userList.forEach(user -> {

            User dbUser = userRepository.findById(user.getId()).orElse(null);

            if(dbUser == null && userRepository.findByLogin(user.getLogin()).isPresent()){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "login: "+user.getLogin()+" has conflict");
            }

            if(dbUser != null && !dbUser.getLogin().equals(user.getLogin()) && userRepository.findByLogin(user.getLogin()).isPresent()){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "login: "+user.getLogin()+" has conflict");
            }

        });

        userRepository.saveAll(userList);

        return true;

    }

    @Override
    public Page<User> findAllUser(int pageNo, int size, double minSalary, double maxSalary, String sort) {

        // Define the valid sort properties
        List<String> validSortProperties = Arrays.asList("id", "name", "login", "salary");
        if(!validSortProperties.contains(sort.substring(1)) || (!sort.startsWith("+") && !sort.startsWith("-"))){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "invalid sort properties. please use + or _ id,name,login or salary");
        }

        Sort.Direction direction = sort.startsWith("-") ? Sort.Direction.DESC : Sort.Direction.ASC;

        Pageable pageable = PageRequest.of(pageNo, size, direction, sort.substring(1));
        Page<User> userPage = userRepository.findBySalaryBetween(minSalary, maxSalary, pageable);
        return userPage;
    }

    @Override
    public User createUser(User user) {
        if(userRepository.findById(user.getId()).isPresent()){
            throw new ResponseStatusException(HttpStatus.CONFLICT, "given id already exist for another employee");
        }

        if(userRepository.findByLogin(user.getLogin()).isPresent()){
            throw new ResponseStatusException(HttpStatus.CONFLICT, "given login already exist for another employee");
        }
        return userRepository.save(user);
    }

    @Override
    public User updateUser(String id, User user) {
        User dbUser = userRepository.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "user with id: "+ id + "not found!")
        );
        if(!dbUser.getLogin().equals(user.getLogin()) && userRepository.findByLogin(user.getLogin()).isPresent()){
            throw new ResponseStatusException(HttpStatus.CONFLICT, "given login already exist for another employee");
        }
        dbUser.setLogin(user.getLogin());
        dbUser.setName(user.getName());
        dbUser.setSalary(user.getSalary());
        return userRepository.save(dbUser);
    }

    @Override
    public Boolean deleteUser(String id) {
        User user = userRepository.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "user with id: "+ id + " not found!")
        );
        userRepository.delete(user);
        return true;
    }
}

