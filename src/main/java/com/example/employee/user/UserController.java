package com.example.employee.user;

import com.opencsv.exceptions.CsvValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;

@RestController
@RequestMapping("users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    private boolean isUploadInProgress = false;
    private final Object lock = new Object();

    @PostMapping("/upload")
    public ResponseEntity<Boolean> uploadCSV(@RequestParam("file") MultipartFile file) throws IOException, CsvValidationException {

        //not to allow concurrent upload
        synchronized (lock) {
            if (isUploadInProgress) {
                throw new ResponseStatusException(HttpStatus.TOO_MANY_REQUESTS,
                        "Another file upload is in progress. please wait and try again");
            }
            isUploadInProgress = true;
        }

        try {
            return ResponseEntity.ok(userService.readCsv(file));
        } finally {
            isUploadInProgress = false;
        }
    }

    @GetMapping
    public ResponseEntity<Page<User>> findAllUser(@RequestParam double minSalary,
                                            @RequestParam double maxSalary,
                                            @RequestParam int offset,
                                            @RequestParam int limit,
                                            @RequestParam String sort){

        return ResponseEntity.ok(userService.findAllUser(offset, limit, minSalary, maxSalary, sort));
    }

    @PostMapping
    public ResponseEntity<User> createUser(@RequestBody User user){
        return ResponseEntity.ok(userService.createUser(user));
    }

    @PutMapping("/{id}")
    public ResponseEntity<User> updateUser(@PathVariable String id, @RequestBody User user){
        return ResponseEntity.ok(userService.updateUser(id, user));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteUser(@PathVariable String id){
        return ResponseEntity.ok(userService.deleteUser(id));
    }


}
