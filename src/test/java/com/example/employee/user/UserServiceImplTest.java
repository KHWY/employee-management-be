package com.example.employee.user;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @BeforeEach
    void setUp() {
        userService = new UserServiceImpl(userRepository);
    }

    @Test
    void canCreateUser() {

        //given
        User user = new User(
                "testId",
                "testLogin",
                "testName",
                5000
        );

        //when
        userService.createUser(user);

        //then
        ArgumentCaptor<User> userArgumentCaptor =
                ArgumentCaptor.forClass(User.class);

        verify(userRepository).save(userArgumentCaptor.capture());

        User capturedUser = userArgumentCaptor.getValue();

        assertThat(capturedUser).isEqualTo(user);
    }

    @Test
    void canThrowExceptionForIdConflict() {

        //given
        User user = new User(
                "testId",
                "testLogin",
                "testName",
                5000
        );

        User existingUser = new User(
                "testId",
                "testLoginEx",
                "testNameEx",
                5000
        );

        given(userRepository.findById(user.getId())).willReturn(Optional.of(existingUser));

        //when
        //then
        assertThatThrownBy(() -> userService.createUser(user))
                .isInstanceOf(ResponseStatusException.class)
                .hasMessageContaining("given id already exist for another employee");
    }

    @Test
    void canThrowExceptionForLoginConflict() {

        //given
        User user = new User(
                "testId",
                "testLogin",
                "testName",
                5000
        );

        User existingUser = new User(
                "testId",
                "testLoginEx",
                "testNameEx",
                5000
        );

        given(userRepository.findByLogin(user.getLogin())).willReturn(Optional.of(existingUser));

        //when
        //then
        assertThatThrownBy(() -> userService.createUser(user))
                .isInstanceOf(ResponseStatusException.class)
                .hasMessageContaining("given login already exist for another employee");
    }
}