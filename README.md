# employee-management-be



## How to Start Running

- Clone the repository
- You need to install postgresql first
- Create a database called "employee"
- Change the database url, username and password in application.properties file

````
spring.datasource.url= jdbc:postgresql://localhost:5432/employee
spring.datasource.username=
spring.datasource.password=
````
